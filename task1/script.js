init(document.getElementById('first-select'))

function docInit() {
    document.getElementById('main-form').addEventListener("submit",function(e){
        e.preventDefault();
        const selects = e.target.getElementsByTagName('select');
        for (let i=0;0<selects.length;i++){
            let selected = selects[i].selectedOptions;
            console.log("select id",selects[i].id);
            for(let j=0;j<selected.length;j++) {
                console.log("value",selected[j].value)
            };
        }
    })
    loadJSON((data)=>{
        create(document.getElementById('main-form'),data)
    });
}

function init(data){
    // Поулчаем id преобразуемого мультиселекта и список опций
    const   id = data.id, options = data.getElementsByTagName('option');

    // Создаем два списка для выбранных опций и невыбранных
    const selectedNode = getCustomSelect(id+'-selected');
    const unselectedNode = getCustomSelect(id+'-unselected');

    // Добавляем в новые списки опции
    for(let i=0; i<options.length;i++){
        // Генерируем опцию
        const optionNode = createNewOption(options[i]);

        if (options[i].selected){
            selectedNode.append(optionNode)
        } else {
            unselectedNode.append(optionNode)
        }
    }

    // Генерируем управляющие кнопки
    const controlNode = getControlMultiSelect(id);

    // Создаем сам контейнер для списков и кнопок
    const customMultiSelect = document.createElement('div');
    customMultiSelect.classList.add('multi-select');
    // Добавляем все сосзданные элементы в основной контейнер
    customMultiSelect.append(unselectedNode);
    customMultiSelect.append(controlNode);
    customMultiSelect.append(selectedNode);

    // Прячем оригинальный select
    data.style.display = "none";
    // Добавляем кастомный select
    data.after(customMultiSelect);
}

function create(form,data){
    data.forEach(itemSelect => {
        // Создаем два списка для выбранных опций и невыбранных
        const selectedNode = getCustomSelect(itemSelect.id+'-selected');
        const unselectedNode = getCustomSelect(itemSelect.id+'-unselected');
        const options = itemSelect.data;
        // Добавляем в новые списки опции
        for(let i=0; i<options.length;i++){
            // Генерируем опцию
            const optionNode = createNewOption(options[i]);

            if (options[i].selected || options[i].selected === "selected"){
                selectedNode.append(optionNode)
            } else {
                unselectedNode.append(optionNode)
            }
        }
        const select = createSelect(itemSelect);
        // Генерируем управляющие кнопки
        const controlNode = getControlMultiSelect(itemSelect.id);

        // Создаем сам контейнер для списков и кнопок
        const customMultiSelect = document.createElement('div');
        customMultiSelect.classList.add('multi-select');
        // Добавляем все сосзданные элементы в основной контейнер
        customMultiSelect.append(unselectedNode);
        customMultiSelect.append(controlNode);
        customMultiSelect.append(selectedNode);

        // Добавляем кастомный select
        form.append(select);
        form.append(customMultiSelect);
    });
}

function onceSelected(id){
    const moveElements = document.getElementById(id+'-unselected')
        .getElementsByClassName('multi-select__option_select');
    moveOptions(moveElements);
}
function onceDeselected(id){
    const moveElements = document.getElementById(id+'-selected')
        .getElementsByClassName('multi-select__option_select');
    moveOptions(moveElements);
}
function allSelected(id){
    const moveElements = document.getElementById(id+'-unselected')
        .getElementsByClassName('multi-select__option');
    moveOptions(moveElements);
}
function allDeselected(id){
    const moveElements = document.getElementById(id+'-selected')
        .getElementsByClassName('multi-select__option');
    moveOptions(moveElements);
}

function createNewOption(option) {
    let optionNode = document.createElement('div');
    optionNode.classList.add("multi-select__option");
    optionNode.setAttribute('data-value',option.value);
    optionNode.innerText = option instanceof HTMLElement ? option.text : option.name;

    // Событие при единственном нажатии кнопки мыши по опции
    optionNode.addEventListener("click",function (e) {
        this.classList.toggle("multi-select__option_select")
    });
    // СОбытие при двойном клике по опции
    optionNode.addEventListener("dblclick",function (e) {moveOptions(this)});

    return optionNode
}

function getControlMultiSelect(id) {
    const controlNode = document.createElement('div');
    controlNode.classList.add('multi-select__control');
    const onceSelectedButton = document.createElement('button');
    onceSelectedButton.addEventListener('click',(e)=>{e.preventDefault();onceSelected(id)});
    onceSelectedButton.innerText = ">";
    const onceDeselectedButton = document.createElement('button');
    onceDeselectedButton.addEventListener('click',(e)=>{e.preventDefault();onceDeselected(id)});
    onceDeselectedButton.innerText = "<";
    const allSelectedButton = document.createElement('button');
    allSelectedButton.addEventListener('click',(e)=>{e.preventDefault();allSelected(id)});
    allSelectedButton.innerText = ">>";
    const allDeselectedButton = document.createElement('button');
    allDeselectedButton.addEventListener('click',(e)=>{e.preventDefault();allDeselected(id)});
    allDeselectedButton.innerText = "<<";
    controlNode.append(allSelectedButton);
    controlNode.append(onceSelectedButton);
    controlNode.append(onceDeselectedButton);
    controlNode.append(allDeselectedButton);
    return controlNode
}

function getCustomSelect(id) {
    const node = document.createElement('div');
    node.classList.add('multi-select__select');
    node.id = id;
    return node;
}

function moveOptions(options){
    let customId, id, toNode;
    if(options instanceof HTMLElement){
        customId = options.parentElement.id;
    } else if (options instanceof HTMLCollection && options.length>0){
        customId = options[0].parentElement.id;
    } else return;

    if(customId.endsWith('-unselected')){
        id = customId.slice(0, -11);
        toNode = document.getElementById(id+'-selected')
    } else {
        id = customId.slice(0, -9);
        toNode = document.getElementById(id+'-unselected')
    }
    const multiselect = document.getElementById(id);
    if(options instanceof HTMLElement){
        moveOption(options,toNode,multiselect)
    } else if (options instanceof HTMLCollection){
        while (options.length>0){
            moveOption(options[0],toNode,multiselect)
        }
    }
}

function moveOption(option,toNode,multiSelect) {
    if(option instanceof HTMLElement && toNode instanceof HTMLElement){
        const options = multiSelect.options;
        for(let i=0; i< options.length;i++){
            if (options[i].value === option.getAttribute('data-value')){
                options[i].selected = !options[i].selected;
                break;
            }
        }
        option.classList.remove('multi-select__option_select');
        toNode.append(option)
    }
}

function createSelect(data){
    const select = document.createElement('select');
    select.id = data.id;
    select.name = data.name;
    select.multiple = true;
    select.style.display = "none";
    data.data.forEach(item => {
        let option = document.createElement('option');
        option.value = item.value;
        option.name = item.name;
        option.selected = item.selected === "selected";
        select.append(option)
    });
    return select
}

function loadJSON(callback){
    let xobj = new XMLHttpRequest();
    xobj.open('GET', 'data.json', true);
    xobj.onreadystatechange = () => {
        if (xobj.readyState === 4 && xobj.status === 200) callback(JSON.parse(xobj.responseText));
    };
    xobj.send()
};