const express = require('express');
const path = require('path')
const app = express();
const server = require('http').createServer(app);
console.log('Запуск сервера на 3000 порту')
server.listen(3000);
app.use(express.static(path.resolve(__dirname)));
app.get('*', function(req, res) {
    res.sendFile(path.resolve(__dirname,'./index.html'));
});