document.getElementById('toggle-mobi-menu').addEventListener("click",(e)=>{
    e.preventDefault()
    document.getElementById('root').classList.toggle('wrapper_menu_show')
});
let startCoordinateX=0
document.addEventListener("touchstart",(event)=>{
    event.stopPropagation();
    startCoordinateX = event.changedTouches[0].screenX
});
document.addEventListener("touchend",(event)=>{
    event.stopPropagation();
    if(Math.abs(event.changedTouches[0].screenX - startCoordinateX) > 150)
        document.getElementById('root').classList.toggle('wrapper_menu_show')
});

const arrowList = document.getElementsByClassName('mobi-menu__arrow');
for (let i=0;i< arrowList.length;i++){
    //parentNode
    arrowList[i].addEventListener('click',(e)=>{
        e.preventDefault()
        arrowList[i].parentElement.classList.toggle('mobi-menu__item_expand')
    })
}